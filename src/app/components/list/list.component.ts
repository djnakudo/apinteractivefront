import { Component, OnInit } from '@angular/core';
import { Task } from '../../models/task';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']

})
export class ListComponent implements OnInit {
    tasks: Task[] = [];
    pages:number = 0;
    activePage:number=0;
    ngOnInit(){
      this.taskService.getTasks().subscribe(response=>{

        this.tasks = response.result;
        this.pages = response.pages;

      })
    }
    nextPage(page){
      this.taskService.getTasks(page).subscribe(response=>{

        this.tasks = response.result;
        this.pages = response.pages;

      })
    }
    onNewTask(task : Task){
     if([task,...this.tasks].length>10){
       this.pages=this.pages+1;
       this.tasks = [task]

     }else{
      this.tasks = [task,...this.tasks]
      if(!this.pages) this.pages=1;
     }

    }
    deleteTask(id:number):void{
      this.taskService.removeTask(id).subscribe(response=>{
       this.tasks= this.tasks.filter(task=>task.id!=id)

      })
    }
    toggleEdit(id):void{
      let taskIndex = this.tasks.findIndex(task=>task.id==id)
      this.tasks[taskIndex].editList = true;

    }
    editTask(id:number,content:String){

      let taskIndex = this.tasks.findIndex(task=>task.id==id)
      this.tasks[taskIndex].editList = false;
      this.taskService.updateTask({id,content} as Task).subscribe(task=>{
        this.tasks[taskIndex].content = task.content;
      })
    }
    numberToArray(n: number): any[] {
      return Array(n);
    }
  constructor(private taskService:TaskService) { }


}
