import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent implements OnInit {
  currentContent = ""
  @Output() newTask: EventEmitter<Task> = new EventEmitter();
  constructor(private taskService:TaskService) { }
  addTask(content){
    if (!content) {
      alert("Please add a content to the task");
    }else{
      this.taskService.saveTask({content} as Task).subscribe(task=>{
        this.newTask.emit(task);
        this.currentContent=""
      })
    }

  }
  ngOnInit() {
  }

}
