import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Task } from "../models/task";
const httpOptions = {
  headers: new HttpHeaders({ "content-type": "application/json" })
};
@Injectable({
  providedIn: "root"
})
export class TaskService {
  tasksUrl: string = "http://localhost:3000/tasks";
  constructor(private http: HttpClient) {}
  getTasks(page: number=0): Observable<any> {
    return this.http.get<any>(
      `${this.tasksUrl}${page ? `?page=${page}` : ""}`
    );
  }
  saveTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.tasksUrl, task, httpOptions);
  }
  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(
      `${this.tasksUrl}/${task.id}`,
      task,
      httpOptions
    );
  }
  removeTask(task: Task | number): Observable<Task> {
    return this.http.delete<Task>(
      `${this.tasksUrl}/${typeof task == "number" ? task : task.id}`,
      httpOptions
    );
  }
}
